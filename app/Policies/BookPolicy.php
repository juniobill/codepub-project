<?php

namespace CodePub\Policies;

use CodePub\Models\Book;
use CodePub\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy
{
    use HandlesAuthorization;

    /**
     * Verifica se o usuario eh dono do livro
     *
     * @param User $user
     * @param Book $book
     * @return bool
     */
    public function manage(User $user, Book $book)
    {
        //posso ter manage em varias policies e o laravel vai saber qual chamar
        //de acordo com o objeto que eh passado no segundo parametro

        //esse eh uma habilidade para verificar se o cara eh dono de algum livro
        return $user->id == $book->user_id;
    }

    public function before(User $user, $ability)
    {
        if ($user->can('book_manage_all')) {
            return true;
        }
    }

}
